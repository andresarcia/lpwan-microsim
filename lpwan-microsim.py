# simulation of spectrum congestion in 868 MHz in one channel


import numpy as np
import math as m
import matplotlib.pyplot as plt

timespan = 60*1000; # in milliseconds
timeinterval = 10;

n_slots = timespan/timeinterval;

channel_width = 125000; #in hertz

most_rob_modulation = 0;
most_eff_modulation = 5;
n_modulations = most_eff_modulation - most_rob_modulation+1;

#TODO: add the three public channels to provide a better perspective of
#TODO: collisions.
#TODO: selection of the channels must be random (868.1. 868.3, 868.5)

n_devices = 1000;
stepsize = 1;

n_packets = 1;

# TODO: add packet size and calculate the matrix dynamically
lora_transmission_delay = np.array([[12, 293, 682],
                            [11, 547, 365],
                            [10, 976, 204],
                            [9, 1757, 113],
                            [8, 3125, 64],
                            [7, 5478, 36],
                            [6, 9375, 21]]);
#class Channel_Emulation:
#Extract third column:
packet_transmission_delay = lora_transmission_delay[:,2];

results = np.zeros((n_devices, 2));
# first column:
# second column:
for curr_n_devices in range(2, n_devices, 1):
    #maps the channel occupation in slots of "timeinterval" to check collisions with other packets
    collisions_mapper = np.zeros((n_slots, n_modulations));
    #remembers the device ID involved
    second_device_mapper =  np.zeros((n_slots, n_modulations));
    #total number of collisions per packet per device
    total_collisions_counter = np.zeros((curr_n_devices, n_packets));
    #calculate the spreading factor for each packet
    sf = np.random.randint(most_rob_modulation, most_eff_modulation+1, (curr_n_devices));
    #transmit all packets in a single SF
    #sf = np.full(curr_n_devices, most_eff_modulation);

    #send a each packet per device "i" and evaluate the collisions
    for i in range(0, curr_n_devices, 1):
        for curr_packet in range(1, n_packets+1, 1): # there is just one packet (TODO: enhance to many packets and each with a different SF)
                                                     # also implement the ADR
            # pickup a random transmission time between 0 and n_slots-packet_transmission_delay
            transmission_time = int(m.floor((n_slots - (packet_transmission_delay[sf[i]]*n_packets/timeinterval)) * np.random.random_sample()));
            # TODO: check this model of overlapping, which does not seem to make sense
            # check overlapping: [-1 0 1] means 1 slot after and before the packet for spillover
            # check overlapping: [0] means no overlapping
            for k in [0]:
                if ((sf[i]+k < most_rob_modulation) or (sf[i]+k > most_eff_modulation)):
                    continue;
                duration  = lora_transmission_delay[sf[i]][2];
                #mapping each slot of the packet
                for j in range(0, int(m.floor(duration/timeinterval)),1):
                    if j+transmission_time > n_slots:
                        continue;
                    if (collisions_mapper[transmission_time + j][sf[i]] == 0):
                        collisions_mapper[transmission_time + j][sf[i]] = 1;
                        second_device_mapper[transmission_time + j][sf[i]] = i;
                    else:
                        collisions_mapper[transmission_time + j][sf[i]] += 1;
                        total_collisions_counter[i] = 1;
                        total_collisions_counter[int(second_device_mapper[transmission_time + j][sf[i]])] = 1;
    # results
    results[curr_n_devices][0] = np.sum(total_collisions_counter);
    results[curr_n_devices][1] = (np.sum(total_collisions_counter)/curr_n_devices)*100;

#print results;

dev = range(2, n_devices+2,1);

fig, ax1 = plt.subplots()
ax1.plot(dev, results[:,0], 'b-')
ax1.set_xlabel('# devices at 1 pkt/min')
# Make the y-axis label, ticks and tick labels match the line color.
ax1.set_ylabel('toal number of collisions', color='b')
ax1.tick_params('y', colors='b')

ax2 = ax1.twinx()
ax2.plot(dev, results[:,1], 'r-')
ax2.set_ylabel('Packet Error Rate (%)', color='r')
ax2.tick_params('y', colors='r')
plt.title("LoRa congestion for single 125kHz channel all devices with random SF")

fig.tight_layout()
plt.show()
