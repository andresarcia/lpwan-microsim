# simulation of spectrum congestion in 868 MHz in one channel
# sigfox

import numpy as np
import math as m
import matplotlib.pyplot as plt
import sys

timespan = 60*1000; # in milliseconds
timeinterval = 10;

n_slots = int(timespan/timeinterval);

#sigfox configuration
frequency_span = 200000; #in hertz, from 868.0 MHz to 868.3 MHz
frequency_interval = 100; #channel width

n_channels = int(m.ceil(frequency_span/frequency_interval));

n_devices = 1000;
dev_interval = 10;
devices_stepsize = int(n_devices/dev_interval);
packet_duration = 2000;
n_packets = 4; # number of retransmissions per packet

results = np.zeros((n_devices/devices_stepsize, 3));
# first column:
# second column:

n = n_devices / devices_stepsize;
total_iterations = 10*(n*(n+1)/2)*n_packets*3*int(m.floor(packet_duration/timeinterval));
print "Total iterations: "+str(total_iterations);
print "Progress: ",
progress_report=0.05; # progress proportion to report
total_progress_acc=0;
progress=0;

for curr_n_devices in range(devices_stepsize, n_devices+1, devices_stepsize):
    #maps the channel occupation in slots of "timeinterval" to check collisions with other packets
    collisions_mapper = np.zeros((n_slots, n_channels));
    #remembers the device ID involved
    second_device_mapper =  np.zeros((n_slots, n_channels));
    #total number of collisions per packet per device
    total_collisions_counter = np.zeros((curr_n_devices, n_packets));
    #calculate the spreading factor for each packet
    freq = np.random.randint(1, n_channels, (curr_n_devices, n_packets));
    # pickup a random transmission time between 0 and n_slots-packet_transmission_delay
    transmission_time = int(m.floor((n_slots - (packet_duration*n_packets/timeinterval))));
    # * np.random.random_sample()) // deleted
    time = np.random.randint(1, transmission_time, (curr_n_devices));
    #transmit all packets in a single SF
    #sf = np.full(curr_n_devices, most_eff_modulation);

    #send a each packet per device "i" and evaluate the collisions
    for i in range(0, curr_n_devices, 1):
        transmission_time = time[i];
        for p in range(0,n_packets):
            for k in range(-1,2,1):
                if ((freq[i][p]+k < 1) or (freq[i][p]+k >= n_channels)):
                    continue;
                #mapping each slot of the packet
                for j in range(0, int(m.floor(packet_duration/timeinterval)),1):
                    if j+transmission_time > n_slots:
                        continue;
                    progress+=1;
                    if ((progress/total_iterations) - (total_progress_acc)) > progress_report:
                        total_progress_acc+=progress_report;
                        print "\r\r\r\r\r"+str(total_progress_acc)+"   ",
                        sys.stdout.flush()

                    if (collisions_mapper[transmission_time + j][freq[i][p]+k] == 0):
                        collisions_mapper[transmission_time + j][freq[i][p]+k] = 1;
                        second_device_mapper[transmission_time + j][freq[i][p]+k] = i;
                    else:
                        collisions_mapper[transmission_time + j][freq[i][p]+k] += 1;
                        total_collisions_counter[i][p] = 1;
                        total_collisions_counter[int(second_device_mapper[transmission_time + j][freq[i][p]+k])][p] = 1;

            # update transmission time to send following retransmission
            transmission_time = int(m.ceil(transmission_time + packet_duration/timeinterval))
    # results
    results[int(m.floor(curr_n_devices/devices_stepsize))-1][0] = np.sum(total_collisions_counter)/2; #lower bound FIX
    # failures
    # print total_collisions_counter;

    failures = total_collisions_counter.sum(axis=1);

    # print failures;

    f=failures[failures == n_packets].sum()/n_packets;
    results[int(m.floor(curr_n_devices/devices_stepsize))-1][1] = f;
    results[int(m.floor(curr_n_devices/devices_stepsize))-1][2] = (f/curr_n_devices)*100;

#print results
dev = range(devices_stepsize, n_devices+1, devices_stepsize);
fig, ax1 = plt.subplots();
a, = ax1.plot(dev, results[:,0], 'b-.', label='total collisions');
b, = ax1.plot(dev, results[:,1], 'b-', label='total failures');

ax1.set_xlabel('# devices at 1 pkt/min');
#Make the y-axis label, ticks and tick labels match the line color.
ax1.set_ylabel('total number of collisions', color='b');
ax1.tick_params('y', colors='b');

ax2 = ax1.twinx();
c, = ax2.plot(dev, results[:,2], 'r-', label='packet error rate');
ax2.set_ylabel('Packet Error Rate (%)', color='r');
ax2.tick_params('y', colors='r');
plt.title("SigFox congestion for 100Hz channels (4 retries) - 3 channels per packet")

plt.legend([a,b,c], ['total collisions','total failures','packet error rate'], loc=2)
fig.tight_layout()
plt.show()

#print results[:][2];
